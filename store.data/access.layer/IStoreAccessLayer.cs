using System;

namespace store.data
{
    public interface IStoreDataAccess
    {
        object GetData();
    }
}
