using System;

namespace store.models
{
    public class BookEvent
    {
        public Guid Id { get; set; } = new Guid();

        public string Version { get; set; } = "v1";

        public Guid BookId { get; set; }

        public string Name { get; set; }

        public DateTime Published { get; set; }

        public decimal Price { get; set; }
    }
}
