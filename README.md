# Book shop

## Description

We would like to have a store for all digital and physical books and novels. For a store we need to have:

- store
- admin
- sales department
- emails department
- vouchers department
- codes for books

Each one of them is going to be separate service with it's own.

## First steps

I'll start with creating a store and than divide domains as I go. There is no need to overly complicate things from the beginning.
