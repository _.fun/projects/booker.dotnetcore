.PHONY: build
build: 
	docker image build --pull -t store .

.PHONY: run
run:
	docker run --rm -it -p 9000:80 store

.Phone: k8sbuild
k8sbuild:
	helm upgrade aspnet3release ./charts/store/

.PHONY: k8s
k8s:
	kubectl port-forward service/aspnet3release-store 9001:80
